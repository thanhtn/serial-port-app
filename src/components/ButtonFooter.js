import React from "react";
import PropTypes from "prop-types";

function ButtonFooter(props) {
  return (
    <div className="button-footer">
      <button
        disabled={!props.canStart}
        onClick={props.onStart}
        className="btn btn-lg btn-primary btn-inventory text-uppercase col-lg-3"
      >
        Inventory
      </button>
      <button
        disabled={!props.canStop}
        onClick={props.onStop}
        className="btn btn-lg btn-danger btn-stop text-uppercase col-lg-3"
      >
        Stop
      </button>
    </div>
  );
}

ButtonFooter.propTypes = {
  canStart: PropTypes.bool,
  onStart: PropTypes.func,
  canStop: PropTypes.bool,
  onStop: PropTypes.func
};
export default ButtonFooter;
