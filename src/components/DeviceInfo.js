import React from "react";
import PropTypes from "prop-types";
import Modal from "react-modal";

function DeviceInfo(props) {
  return (
    <Modal
      ariaHideApp={false}
      isOpen={props.isOpen}
      style={customStyles}
      onRequestClose={props.onRequestClose}
    >
      <div className="modal-header">
        <h5 className="modal-title" id="exampleModalLongTitle">
          Reader's Info
        </h5>
        <button
          type="button"
          className="close"
          onClick={() => props.onRequestClose()}
        >
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div className="modal-body">
        <br></br>
        <label>
          Reader's Info: <span>{props.infor}</span>
        </label>
      </div>
      <div className="modal-footer">
        <button
          type="button"
          className="btn btn-secondary btn-close"
          onClick={() => props.onRequestClose()}
        >
          Close
        </button>
      </div>
    </Modal>
  );
}

DeviceInfo.propTypes = {
  isOpen: PropTypes.bool,
  infor: PropTypes.string,
  onRequestClose: PropTypes.func
};
export default DeviceInfo;

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)"
  }
};
