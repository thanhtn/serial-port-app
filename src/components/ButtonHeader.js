import React from "react";
import PropTypes from "prop-types";

function ButtonHeader(props) {
  return (
    <div className="button-header">
      <button
        onClick={() => props.connect()}
        className="btn btn-lg btn-connect text-uppercase col-lg-5"
      >
        Connect New Device
      </button>
      <button
        className="btn btn-lg btn-light btn-render text-uppercase col-lg-5"
        type="submit"
        disabled={!props.canGetInfo}
        onClick={() => props.getInfo()}
      >
        Reader's Info
      </button>
    </div>
  );
}

ButtonHeader.propTypes = {
  canGetInfo: PropTypes.bool,
  getInfo: PropTypes.func,
  connect: PropTypes.func
};
export default ButtonHeader;
