import React from "react";
import PropTypes from "prop-types";
import ListGroup from "react-bootstrap/ListGroup";

function ListItem(props) {
  return (
    <ListGroup>
      {props.items.map((item, index) => (
        <ListGroup.Item key={index}>
          <div className="data">
            <div className="row">
              {item[3] ? (
                <div className="col-10 col-sm-9  col-md-10 col-lg-10 highlight-item">
                  {item[0]}
                </div>
              ) : (
                <div
                  onClick={() => props.onItemClick(index)}
                  className="col-10 col-sm-9  col-md-10 col-lg-8"
                >
                  {item[0]}
                </div>
              )}
              <div className="col-2 col-sm-3 col-md-2 col-lg-2">{item[2]}</div>
              <div className="col-2 col-sm-3 col-md-2 col-lg-2">{item[1]}</div>
            </div>
          </div>
        </ListGroup.Item>
      ))}
    </ListGroup>
  );
}

ListItem.propTypes = {
  items: PropTypes.array,
  onItemClick: PropTypes.func
};
export default ListItem;
