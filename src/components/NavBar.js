import React from "react";
import { Link } from "react-router-dom";
import "../App.css";

const NavBar = () => (
  <div className="row head-nav">
    <div className="col-md-6">
      <Link to="/">Home</Link>
    </div>
    <div className="col-md-6">
      <Link to="/import">Import</Link>
    </div>
  </div>
);

export default NavBar;
