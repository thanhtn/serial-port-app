import React from "react";
import PropTypes from "prop-types";
import Modal from "react-modal";

function DeviceDetail(props) {
  return (
    <Modal
      ariaHideApp={false}
      isOpen={props.isOpen}
      style={customStyles}
      onRequestClose={() => props.onRequestClose()}
    >
      <div className="modal-body">
        <br></br>
        <label>
          TagId: <span>{props.tagId}</span>
        </label>
        <p>
          dBm: <span>{props.dBm}</span>
        </p>
      </div>
      <div className="modal-footer">
        <button
          type="button"
          className="btn btn-secondary btn-close"
          onClick={() => props.onRequestClose()}
        >
          Close
        </button>
      </div>
    </Modal>
  );
}

DeviceDetail.propTypes = {
  isOpen: PropTypes.bool,
  tagId: PropTypes.string,
  dBm: PropTypes.number,
  onRequestClose: PropTypes.func
};
export default DeviceDetail;

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)"
  }
};
