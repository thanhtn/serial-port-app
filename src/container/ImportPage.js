import React, { useState, useRef, useEffect } from "react";
import "../App.css";
import ListItem from "../components/ListItem";
import CSVReader from "react-csv-reader";
import DeviceInfo from "../components/DeviceInfo";
import ButtonFooter from "../components/ButtonFooter";
import ButtonHeader from "../components/ButtonHeader";

const ImportPage = () => {
  const [data, setData] = useState([]);
  const [count, setCount] = useState(0);
  const [countAll, setCountAll] = useState(0);
  const [isModalShow, setModalShow] = useState(false);
  const [canStart, setCanStart] = useState(false);
  const [canStop, setCanStop] = useState(false);
  const [canGetInfo, setCanGetInfo] = useState(false);
  const encoder = new TextEncoder(); // always utf-8
  const decoder = new TextDecoder("utf-8");
  var combied_value = "";
  const chunks = [];
  var hasNewChunk = false;
  var isDeviceInfo = false;
  var d_version = "";
  var isTagInfo = false;
  const [port, setPort] = useState(null);
  const [writer, setWriter] = useState(null);
  const [device_version, setVersion] = useState("");
  const [needCheck, setNeedCheck] = useState(true);

  const messagesEndRef = useRef(null);

  const checkNeedScan = () => {
    if (data.length === 0) return;
    let needScan = false;
    for (let i = 0; i < data.length; i++) {
      if (!data[i][2]) {
        needScan = true;
        break;
      }
    }
    if (!needScan) {
      //TODO set alert
      setNeedCheck(false);
      onStop();
      return;
    }
  };

  useEffect(checkNeedScan, [data]);

  useEffect(() => {
    return () => {
      console.log("ImportPage unmount");
      if (port) port.close();
    };
  }, [port]);

  function connectPort() {
    if (data.length === 0) {
      alert("Please import file first");
      return;
    }
    const requestOptions = {
      // Filter on devices with the Arduino USB vendor ID.
      filters: [{ vendorId: 0x2341 }]
    };
    navigator.serial.requestPort(requestOptions).then(port => {
      port.open({ baudrate: 115200 }).then(() => {
        const writer = port.writable.getWriter();
        setPort(port);
        setWriter(writer);
        let str = encoder.encode("~wk1\r\n");

        console.log(port);

        port.readable.pipeTo(
          new WritableStream({
            write(chunk) {
              let value = decoder.decode(chunk).toString();
              handleChunk(value);
            },
            close() {
              console.log("All data successfully read!");
            },
            abort(e) {
              console.error("Something went wrong!", e);
            }
          })
        );

        setInterval(() => {
          if (hasNewChunk) {
            hasNewChunk = false;
            if (isDeviceInfo) {
              setVersion(d_version);
            } else {
              //hight light item
              const newData = [...data];
              for (let i = 0; i < chunks.length; i++) {
                let foundIndex = data.findIndex(item => item[0] === chunks[i]);
                if (foundIndex !== -1) {
                  newData[foundIndex][2] = true;
                  setData(newData);
                  setCount(newData.filter(item => item[2] === true).length);
                }
              }
            }
          }
        }, 1);
        writer.write(str);
      });
    });
  }

  const handleChunk = value => {
    switch (value) {
      case "~Af0000\r\n":
        console.log("start data");
        isTagInfo = true;
        updateButtonState(true);
        break;
      case "~As0000\r\n":
        console.log("end data");
        isTagInfo = false;
        updateButtonState(false);
        break;
      case "~Wk0000\r\n":
        updateButtonState(false);
        break;
      default:
        if (!isTagInfo) {
          console.log("GetInfo = " + value);
          isDeviceInfo = true;
          d_version = value;
        } else {
          isDeviceInfo = false;
          value = combied_value + value;
          if (value.endsWith("\r\n")) {
            let new_value_arr = value
              .replace(/~eT3000/g, "")
              .replace(/~eT7000/g, "")
              .replace(/~eT3400/g, "")
              .replace(/~eT/g, "")
              .split(/\r\n/);
            new_value_arr.forEach(i => {
              // console.log(i);
              if (i === "~As0000") {
                updateButtonState(false);
              } else if (i === "~Af0000") {
                updateButtonState(true);
              } else if (i !== "") {
                let foundIndex = chunks.findIndex(item => item[0] === i);
                if (foundIndex === -1) {
                  chunks.push(i);
                }
              }
            });
            combied_value = "";
          } else {
            combied_value = value;
          }
        }
        hasNewChunk = true;
        break;
    }
  };

  function updateButtonState(isScanning) {
    setCanStart(!isScanning);
    setCanStop(isScanning);
    setCanGetInfo(!isScanning);
  }

  function onInventory() {
    if (writer) {
      let str = encoder.encode("~af\r\n");
      writer.write(str);
    }
  }

  function onStop() {
    if (writer) {
      let str = encoder.encode("~as\r\n");
      writer.write(str);
      updateButtonState(false);
    }
  }

  const onParseSuccess = result => {
    const arrCsv = [...result];
    arrCsv.splice(0, 1);
    for (let i = 0; i < arrCsv.length; i++) {
      arrCsv[i].push(false);
    }
    setData(arrCsv);
    setCountAll(arrCsv.length);
  };

  const onParseError = () => {
    alert("Parse File Error");
  };

  function closeModal() {
    setModalShow(false);
  }

  function openModal() {
    if (writer) {
      getInfo();
      setModalShow(true);
    }
  }

  function getInfo() {
    let str = encoder.encode("~rv\r\n");
    writer.write(str);
  }

  function onItemClick(index) {
    let item = { ...data[index] };
    console.log(item);
  }

  return (
    <div className="form-signin">
      <div>
        <CSVReader
          cssClass="csv-reader-input"
          onFileLoaded={onParseSuccess}
          onError={onParseError}
          inputId="csvfile"
          inputStyle={{ color: "red" }}
        />
      </div>
      <p />
      <ButtonHeader
        canGetInfo={canGetInfo}
        getInfo={openModal}
        connect={connectPort}
      />
      <DeviceInfo
        isOpen={isModalShow}
        infor={device_version}
        onRequestClose={closeModal}
      />
      {!needCheck && <h5 className="title-alert">All tags have been found</h5>}
      <div className="form-list">
        <ListItem items={data} onItemClick={onItemClick} />
        <div ref={messagesEndRef} />
      </div>
      <p>
        Number of tags found : <span>{count}</span>
        <br />
        All tag in csv file: <label className="count">{countAll}</label>
      </p>
      <p></p>
      <ButtonFooter
        canStart={canStart}
        onStart={onInventory}
        canStop={canStop}
        onStop={onStop}
      />
    </div>
  );
};

export default ImportPage;
