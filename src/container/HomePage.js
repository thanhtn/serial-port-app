import React, { useState, useRef, useEffect } from "react";
import "../App.css";
import "react-toggle/style.css";
import Toggle from "react-toggle";
import { CSVLink } from "react-csv";
import ListItem from "../components/ListItem";
import DeviceInfo from "../components/DeviceInfo";
import DeviceDetail from "../components/DeviceDetail";
import ButtonHeader from "../components/ButtonHeader";
import ButtonFooter from "../components/ButtonFooter";

const HomePage = props => {
  //state
  const [data, setData] = useState([]);
  const [count, setCount] = useState(0);
  const [countAll, setCountAll] = useState(0);
  const [showInfor, setShowInfor] = useState(false);
  const [canStart, setCanStart] = useState(false);
  const [canStop, setCanStop] = useState(false);
  const [canGetInfo, setCanGetInfo] = useState(false);
  const [canSwitch, setCanSwitch] = useState(true);
  const [tagIdFilter, setTagFilter] = useState("");
  const [showFilter, setShowFilter] = useState(false);
  const [port, setPort] = useState(null);
  const [writer, setWriter] = useState(null);
  const [device_version, setVersion] = useState("");
  const [dBm, setdBm] = useState(0);
  const [isRSSI, setRSSI] = useState(false);
  //end state
  let is_rssi = false;

  const encoder = new TextEncoder(); // always utf-8
  const decoder = new TextDecoder("utf-8");
  let combied_value = "";
  const chunks = [];
  let hasNewChunk = false;
  let isDeviceInfo = false;
  let d_version = "";
  let isTagInfo = false;
  let count_all = 0;

  const messagesEndRef = useRef(null);

  const csvHeaders = ["TagId", "Count"];

  /* const scrollToBottom = () => {
    if (data.length > 0) messagesEndRef.current.scrollIntoView();
    // messagesEndRef.current.scrollTop = messagesEndRef.current.scrollHeight;
  };

  useEffect(scrollToBottom, [data]); */

  useEffect(() => {
    return () => {
      console.log("ImportPage unmount");
      if (port) port.close();
    };
  }, [port]);

  useEffect(() => {
    let id = setInterval(() => {
      if (showFilter) {
        console.log(data);
      }
    }, 1);
    return () => {
      console.log("Clear");
      clearInterval(id);
    };
  }, [showFilter, data]);

  useEffect(() => {
    const id = setInterval(() => {
      if (showFilter) {
        const filterArr = data.filter(item => item[0] === tagIdFilter);
        console.log("check dBm");
        console.log(filterArr);
        setdBm(filterArr[0][2]);
        console.log(data.length);
      }
    }, 1);

    return () => {
      // if (tagIdFilter === "") {
      console.log("clear interval");
      clearInterval(id);
      // }
    };
  }, [showFilter, data, tagIdFilter]);

  async function connectPort() {
    const requestOptions = {
      // Filter on devices with the Arduino USB vendor ID.
      filters: [{ vendorId: 0x2341 }]
    };
    const port = await navigator.serial.requestPort(requestOptions);
    await port.open({ baudrate: 115200 });
    /* navigator.serial.requestPort(requestOptions).then(port => {
      port.open({ baudrate: 115200 }).then(() => { */
    const writer = port.writable.getWriter();
    setPort(port);
    setWriter(writer);
    let str = encoder.encode("~wk1\r\n");
    // let str = encoder.encode("~w81\r\n");
    console.log(port);

    const reader = port.readable.getReader();
    writer.write(str);
    while (true) {
      const response = await reader.read();
      if (response.done) break;
      let value = decoder.decode(response.value);
      handleChunk(value);
    }

    /* port.readable.pipeTo(
      new WritableStream({
        write(chunk) {
          let value = decoder.decode(chunk).toString();
          handleChunk(value);
        },
        close() {
          console.log("All data successfully read!");
        },
        abort(e) {
          console.error("Something went wrong!", e);
        }
      })
    ); */

    // writer.write(str);
    /* });
    }); */
  }

  setInterval(() => {
    if (hasNewChunk) {
      hasNewChunk = false;
      if (isDeviceInfo) {
        setVersion(d_version);
      } else {
        setData([...chunks]);
        setCount(chunks.length);
        setCountAll(count_all);
      }
    }
  }, 1);

  const handleChunk = value => {
    switch (value) {
      case "~Af0000\r\n":
        console.log("start data");
        isTagInfo = true;
        updateButtonState(true);
        break;
      case "~As0000\r\n":
        console.log("end data");
        isTagInfo = false;
        updateButtonState(false);
        break;
      case "~Wk0000\r\n":
        if (is_rssi) {
          onClear();
        }
        console.log("can start Wk0000");
        updateButtonState(false);
        is_rssi = false;
        break;
      case "~W80000\r\n":
        if (!is_rssi) {
          onClear();
        }
        console.log("can start W80000");
        updateButtonState(false);
        is_rssi = true;
        break;
      default:
        if (!isTagInfo) {
          console.log("GetInfo = " + value);
          isDeviceInfo = true;
          d_version = value;
        } else {
          isDeviceInfo = false;
          value = combied_value + value;
          // console.log(value + "---");
          if (value.endsWith("\r\n")) {
            let new_value_arr = value
              .replace(/~eT3000/g, "")
              .replace(/~eT7000/g, "")
              .replace(/~eT3400/g, "")
              .replace(/~eT/g, "")
              .split(/\r\n/);
            new_value_arr.forEach(i => {
              // console.log(i);
              let i_arr = i.split(",");
              let tagId = i_arr[0];
              if (tagId === "~As0000") {
                isTagInfo = false;
                updateButtonState(false);
              } else if (tagId === "~Af0000") {
                isTagInfo = true;
                updateButtonState(true);
              } else if (tagId !== "") {
                let foundIndex = chunks.findIndex(item => item[0] === tagId);
                let rssi = (parseInt(i_arr[1], 16) - 0x10000) / 10;
                if (foundIndex === -1) {
                  chunks.push([tagId, 1, rssi]);
                } else {
                  chunks[foundIndex][1] += 1;
                  chunks[foundIndex][2] = rssi;
                }
                count_all++;
              }
            });
            combied_value = "";
          } else {
            combied_value = value;
          }
        }
        hasNewChunk = true;
        break;
    }
  };

  function updateButtonState(isScanning) {
    setCanStart(!isScanning);
    setCanStop(isScanning);
    setCanSwitch(!isScanning);
    setCanGetInfo(!isScanning);
  }

  function onInventory() {
    if (writer) {
      let str = encoder.encode("~af\r\n");
      writer.write(str);
    }
  }

  function onStop() {
    if (writer) {
      let str = encoder.encode("~as\r\n");
      writer.write(str);
      updateButtonState(false);
    }
  }

  function closeModalInfor() {
    setShowInfor(false);
  }

  function openModalInfor() {
    setShowInfor(true);
    if (writer) {
      getInfo();
      setShowInfor(true);
    }
  }

  function getInfo() {
    let str = encoder.encode("~rv\r\n");
    writer.write(str);
  }

  function onItemClick(index) {
    let item = { ...data[index] };
    console.log(item);
    setTagFilter(item[0]);
    setdBm(item[2]);
    openModalFilter();
  }

  function openModalFilter() {
    setShowFilter(true);
    // let str = encoder.encode("~wk1\r\n");
    // writer.write(str);
  }

  function closeModalFilter() {
    setShowFilter(false);
    setTagFilter("");
  }

  function onClear() {
    chunks.splice(0);
    count_all = 0;
    setCount(0);
    setCountAll(0);
    setData(chunks);
  }

  /* const onTest = () => {
    const d = [...data];
    setData(d);
    d.push(["abc", 1]);
  }; */

  function switchRSSI(e) {
    let status = e.target.checked;
    let cmdLine = "";
    if (writer) {
      if (status) {
        cmdLine = "~w81\r\n";
      } else {
        cmdLine = "~wk1\r\n";
      }
      writer.write(encoder.encode(cmdLine));
    } else {
      alert("Please connect new device");
      status = false;
    }
    setRSSI(status);
  }

  return (
    <div className="form-signin">
      <ButtonHeader
        canGetInfo={canGetInfo}
        getInfo={openModalInfor}
        connect={connectPort}
      />
      <div className="text-center">
        <label>
          <div className="text-uppercase">
            <b>RSSI</b>
          </div>
          <Toggle
            checked={isRSSI}
            className="custom-classname"
            onChange={switchRSSI}
            disabled={!canSwitch}
          />
        </label>
      </div>
      <DeviceInfo
        isOpen={showInfor}
        infor={device_version}
        onRequestClose={closeModalInfor}
      />
      <DeviceDetail
        isOpen={showFilter}
        tagId={tagIdFilter}
        dBm={dBm}
        onRequestClose={closeModalFilter}
      />
      <div className="form-list">
        <ListItem items={data} onItemClick={onItemClick} />
        <div ref={messagesEndRef} />
      </div>

      <div>
        {data.length > 0 ? (
          <CSVLink
            data={data}
            headers={csvHeaders}
            filename={"data.csv"}
            target="_blank"
            separator={","}
          >
            Export CSV
          </CSVLink>
        ) : null}
      </div>

      <p>
        Count: <span>{count}</span>
        <br />
        Count All: <label className="count">{countAll}</label>
      </p>

      {/* <div className="button-footer">
        <button
          onClick={onClear}
          className="btn btn-lg btn-primary btn-inventory text-uppercase col-lg-3"
        >
          Clear
        </button>
        <button
          onClick={onTest}
          className="btn btn-lg btn-primary btn-inventory text-uppercase col-lg-3"
        >
          Test
        </button>
      </div> */}

      <ButtonFooter
        canStart={canStart}
        onStart={onInventory}
        canStop={canStop}
        onStop={onStop}
      />
    </div>
  );
};

export default HomePage;
