import PropTypes from "prop-types";
import { ConnectedRouter } from "connected-react-router";
import routes from "./routes";
import React from "react";
import "./App.css";

const App = ({ history }) => {
  return (
    <div className="container">
      <div className="row">
        <div className="col-sm-9 col-md-7 col-lg-8 mx-auto">
          <div className="card card-signin my-5">
            <div className="card-body">
              <h5 className="card-title text-center">RF PRISMA C5C6</h5>
              <ConnectedRouter history={history}>{routes}</ConnectedRouter>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

App.propTypes = {
  history: PropTypes.object
};

export default App;
