import React from "react";
import { Route, Switch } from "react-router";
import HomePage from "../container/HomePage";
import ImportPage from "../container/ImportPage";
import NavBar from "../components/NavBar";

const routes = (
  <div>
    <NavBar />
    <br />
    <Switch>
      <Route exact path="/" component={HomePage} />
      <Route path="/import" component={ImportPage} />
    </Switch>
  </div>
);

export default routes;
